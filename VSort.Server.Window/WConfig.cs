﻿using System;
using System.Net;
using System.Windows.Forms;
using VSort.Server.Config;

namespace VSort.Server.Window {
    public partial class WConfig : Form {
        public WConfig() {
            Load += WConfig_Load;
            InitializeComponent();
            }

        private void WConfig_Load(object sender, EventArgs e) {
            textDBHost.Text = "127.0.0.1";
            textDBname.Text = "sort";
            textPort.Text = "6666";
            textUserName.Text = "root";
            textPsw.Text = "root";
            dbPort.Text = "3306";
            string ip = "127.0.0.1";
            IpList.Items.Add(ip);
#pragma warning disable CS0618 // 类型或成员已过时
            IPAddress[] allIp = Dns.Resolve(Dns.GetHostName()).AddressList;
#pragma warning restore CS0618 // 类型或成员已过时
            foreach (IPAddress x in allIp)
            {
                ip = x.ToString();
                IpList.Items.Add(ip);
            }
            }

        private void button2_Click(object sender, EventArgs e) {
            Close();
            }

        private void button3_Click(object sender, EventArgs e) {

            }

        private void button1_Click(object sender, EventArgs e) {
            bool ISNULL = (IpList.Text == "")
                || (dbPort.Text == "")
                || (textPort.Text == "")
                ||(textPsw.Text=="")
                ||(textUserName.Text=="")
                ||(textDBHost.Text=="")
                ||(textDBname.Text=="");
            if (ISNULL) {
                MessageBox.Show("不能为空");
                }
            else {
                if (!ConfigControl.setConfigInfo(IpList.Text, textPort.Text, textUserName.Text, textDBname.Text, textPsw.Text, textDBHost.Text,dbPort.Text))
                    MessageBox.Show("异常!!");
                else
                    MessageBox.Show("设置成功");
                }
            }
        }
    }
