﻿using LitJson;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using VSort.Server.Loger;
using VSort.Server.Server;

namespace VSort.Server.Window {
    public partial class SortServer : Form {
        private R_S_Log log = new R_S_Log();
        private List<Thread> threadPool = new List<Thread>();
        private bool ServerFlag = false;
        JsonData ConfigData=null;
        Thread startServer;
        public SortServer() {
            Load += SortServer_Load;
            Paint += SortServer_Paint;
            FormClosed += SortServer_FormClosed;
            InitializeComponent();
            }

        private void SortServer_FormClosed(object sender, FormClosedEventArgs e) {
            stopAllTask();
            }

        private void SortServer_Paint(object sender, PaintEventArgs e) {
            if (ServerFlag == false)
                button2.Text = "启动服务";
            else
                button2.Text = "停止服务";
            }

        private void SortServer_Load(object sender, EventArgs e) {
            //获得当前IP地址
            ConfigData= Config.ConfigControl.loadConfig();
            if (ConfigData != null)
            {
                string ip = ConfigData["ServerAddr"].ToString();
                HostLabel.Text = ip;
                PortLabel.Text = ConfigData["Port"].ToString();
            }
            log.log("启动服务端");
            
            }

        private void button1_Click(object sender, EventArgs e) {
            stopAllTask();
            Application.Exit();
            }
        private delegate void SetTextValue(Label label,string str);
        private delegate void SetButtonText(SortServer button,string str);
        private void setStatueLabel(Label label,string str) {
            label.Text = str;
            }
        private void setButtonTextValue(SortServer server,string str) {
            server.ServerFlag = false;
            server.Refresh();
            }
        private void button2_Click(object sender, EventArgs e) {
            if (ServerFlag==false) {
                SetTextValue setTextValue = new SetTextValue(setStatueLabel);
                SetButtonText setButtonText = new SetButtonText(setButtonTextValue);
                startServer = new Thread(new ThreadStart(() => {
                    label4.Invoke(setTextValue, label4, "正在启动服务,请稍后...");
                    ServerController server = new ServerController();
                    try {
                        server.setConfig(this.ConfigData["ServerAddr"].ToString(), this.ConfigData["Port"].ToString());
                        PortLabel.Invoke(setTextValue, PortLabel, (server.PORT).ToString());
                        HostLabel.Invoke(setTextValue, HostLabel, (ConfigData["ServerAddr"]).ToString());

                        server.StartListener();
                        label4.Invoke(setTextValue, label4, "正在运行中");
                    }
                    catch (Exception err) {
                        log.error("服务启动错误" + err.Message);
                        }
                }));
                ServerFlag = true;
                threadPool.Add(startServer);
                startServer.IsBackground = true;
                startServer.Start();
                Refresh();
                }
            else {
                label4.Text="服务已关闭";
                stopAllTask();
                ServerFlag = false;
                Refresh();
                }
            }
        private void stopAllTask() {
            foreach (Thread x in threadPool) {
                x.Abort();
                }
            }
        ~SortServer() {
            stopAllTask();
            }

        private void button3_Click(object sender, EventArgs e) {
            WConfig wconfig = new WConfig();
            wconfig.Show();
            }
        }
    }
