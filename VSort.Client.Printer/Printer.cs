﻿using System;
using Microsoft.Reporting.WebForms;
using System.Drawing.Printing;
using VSort.Client.Loger;
namespace VSort.Client.Printer {
    public class TicketPrinter {
        LocalReport report = new LocalReport();
        R_C_Log log = new R_C_Log();
        /// <summary>
        /// 报表类，生成并打印排号单
        /// </summary>
        public TicketPrinter ( ) {
            report.ReportPath = @"Ticket.rdlc";
        }

        /// <summary>
        /// 排号单号，名称，组织
        /// 并且在最后插入排号单生成时间及有效时间
        /// </summary>
        /// <param name="Number"></param>
        /// <param name="Name"></param>
        /// <param name="Org"></param>
        public void printTicket ( string Number, string Name,string Org ) {
            DateTime dt = DateTime.Now;
            ReportParameter[] Data = {
                new ReportParameter("Number",Number),
                new ReportParameter("Name",Name),
                new ReportParameter("Org",Org),
                new ReportParameter("StartTime",dt.ToLocalTime().ToString()),
                new ReportParameter("StartTime",dt.AddHours(24).ToString())
                };
            report.SetParameters ( Data );
            report.Refresh ( );
            PrintDocument printTicket = new PrintDocument();
            var Printers = PrinterSettings.InstalledPrinters;
            foreach ( string getPrienterName in Printers ) {
                printTicket.PrinterSettings.PrinterName = getPrienterName;
                try {
                    //TODO:尝试连接打印机打印客户回执单
                    break;
                }
                catch ( Exception e ) {
                    log.S_Log ( e.Message );
                    continue;
                }
            }
        }
    }
}
