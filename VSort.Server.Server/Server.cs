﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Speech.Synthesis;
using System.Text;
using VSort.Server.Loger;
using VSort.Server.Data;

namespace VSort.Server.Server {
    using Config;
    using LitJson;
    using Newtonsoft.Json.Linq;
    using System.IO;

    public class StateObject {
        public Socket workSocket = null;
        public const int BufferSize = 1024;
        public byte[] buffer = new byte[BufferSize];
        public StringBuilder sb = new StringBuilder();
    }
    /// <summary>
    /// 服务器SOCKET连接类
    /// </summary>
    public class ServerController {
        public  int PORT;
        //TODO: 本地回环
        private static SpeechSynthesizer Speeker = new SpeechSynthesizer();
        private IPAddress HOST;
        private static R_S_Log log = new R_S_Log();
        private static ManualResetEvent Reseter = new ManualResetEvent(false);
        private static StringBuilder MSG = new StringBuilder();
        private static DataControl Data;
        private ConfigControl COnfig = new ConfigControl();
        /// <summary>
        /// 初始化类，添加服务器地址和端口
        /// </summary>
        /// <param name="HOST"></param>
        /// <param name="PORT"></param>
        public ServerController ( string Host = null, int Port = 6666 ) {
            if (Host != null)
                HOST = IPAddress.Parse(Host);
                else
                HOST = IPAddress.Parse("127.0.0.1");
            PORT = Port;
        }
        public void setConfig(string Host, string Port)
        {
            HOST = IPAddress.Parse(Host);
            PORT = int.Parse(Port);
        }
        private Socket CreateListener ( ) {
            Socket Listener = null;
            Listener = new Socket ( AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp );
            try {
                IPEndPoint ServerBind = new IPEndPoint(HOST, PORT);
                Listener.Bind ( ServerBind );
            }
            catch ( Exception e ) {
                log.log ( "服务启动错误" + e.Message );
            }
            return Listener;
        }

        /// <summary>
        /// 启动服务监听
        /// </summary>
        public void StartListener ( ) {
            try {
                Socket Listener = CreateListener();
                Listener.Listen ( 200 );
                while ( true ) {
                    Reseter.Reset ( );
                    //TcpListener T = new TcpListener(HOST, PORT);
                    Listener.BeginAccept ( new AsyncCallback ( GetMessage ), Listener );
                    Reseter.WaitOne ( );
                }
            }
            catch ( Exception e ) {
                log.log ( e.Message );
            }
        }

        /// <summary>
        /// 接收消息的委托方法
        /// </summary>
        /// <param name="Request"></param>
        private void GetMessage ( IAsyncResult Request ) {
            Reseter.Set ( );
            Socket Server = (Socket)Request.AsyncState;
            Socket Service = Server.EndAccept(Request);
            StateObject state = new StateObject();
            state.workSocket = Service;
            Service.BeginReceive ( state.buffer, 0, state.buffer.Length, 0, new AsyncCallback ( ReadMessage ), state );
            Speeker.Speak ( "连接成功" );
        }

        /// <summary>
        /// 读取消息的委托方法
        /// </summary>
        /// <param name="Request"></param>
        private static void ReadMessage ( IAsyncResult Request ) {
            try {
                StateObject state = (StateObject)Request.AsyncState;
                Socket Server = state.workSocket;
                int bytesRead = Server.EndReceive(Request);
                string content = string.Empty;
                if ( bytesRead > 0 ) {
                    state.sb.Clear ( );
                    state.sb.Append ( Encoding.Default.GetString ( state.buffer, 0, bytesRead ) );
                    //content为收到的字节
                    content = state.sb.ToString ( );
                    MSG.Append ( content );
                    /*
                     * TODO:接收处理json
                     * 接收json数据，判断是来自什么用户的json
                     * 数据读取加密盐控制端json通过时间戳AES加
                     * 密生成KEY返回给控制端，若为客户端数据，
                     * 则判断数据的完整性存入数据库。
                     */
                    if ( content.IndexOf ( "<EOF>" ) > -1 ) {
                        Console.WriteLine ( "Read {0} bytes from socket. \n Data : {1}", content.Length, content );
                    }
                    else {
                        Server.BeginReceive ( state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback ( ReadMessage ), state );
                    }
                    //TODO:最后处理收到的json数据
                    //JsonReader reader = new JsonTextReader(new StringReader(MSG.ToString());
                    JsonData json = JsonMapper.ToObject(content.ToString());
                    try
                    {
                        string back = json["msg"].ToString();
                        Send(Server, back);
                    }
                    catch { Speeker.SpeakAsync("只接受哲学"); }
                    
                }
            }
            catch ( Exception e ) {
                log.log ( e.Message );
            }
        }

        /// <summary>
        /// 发送消息的委托方法
        /// </summary>
        /// <param name="Server"></param>
        /// <param name="data"></param>
        private static void Send ( Socket Server, string data ) {
            byte[] byteData = new Byte[1024];
            byteData = Encoding.UTF8.GetBytes ( data );
            Speeker.SpeakAsync ( "发送数据" + data );
            Server.BeginSend ( byteData, 0, byteData.Length, 0, new AsyncCallback ( SendCallback ), Server );
        }
        private static void SendCallback ( IAsyncResult Request ) {
            try {
                Socket Server = (Socket)Request.AsyncState;
                int bytesSent = Server.EndSend(Request);
                Console.WriteLine ( "发送数据" );
            }
            catch ( Exception e ) {
                log.log ( e.Message );
                Console.WriteLine ( e.ToString ( ) );
            }
        }
        ~ServerController ( ) {
            Speeker.Dispose ( );
        }
    }
}
