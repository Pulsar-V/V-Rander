﻿using System;
using System.Text;
using System.IO;

namespace VSort.Client.Loger {
    public class R_C_Log {
        private string Log_Dictory = "log";
        private string Time =DateTime.Now.ToLongDateString();
        private string Log_File_Path = "./log/" + DateTime.Now.ToLongDateString() + ".log";
        private bool Create_Directory() {
            try {
                if (Directory.Exists(Log_Dictory)) {
                    return true;
                    }
                else {
                    Directory.CreateDirectory(Log_Dictory);
                    return true;
                    }
                }
            catch (Exception) {
                return false;
                }
            }
        public bool S_Log(string Data) {
            if (Create_Directory()) {
                Data = string.Format("Time:{0}\nUser: {1}\nOS:{2}\nInfo:\n{3}\n\n", Time,Environment.UserName,Environment.OSVersion,Data);
                //System.Console.WriteLine(Log_File_Path);
                try {
                    StreamWriter F_Writer = new StreamWriter(Log_File_Path, true, Encoding.UTF8);
                    F_Writer.Write(Data);
                    F_Writer.Close();
                    return true;
                    }
                catch (Exception ) {
                    return false;
                    }
                }
            else {
                return false;
                }
            }
        }
    }
