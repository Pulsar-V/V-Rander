﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Speech.Synthesis;
using System.Threading;
using System.Text;
using VSort.Client.Loger;
using VSort.Client.Printer;
using System.Collections.Generic;

namespace VSort.Client.Server {
    public class StateObject {
        public Socket workSocket = null;
        public const int BufferSize = 1024;
        public byte[] buffer = new byte[BufferSize];
        public string Content;
    }

    public class ClientSocket {
        public static ManualResetEvent Reseter = new ManualResetEvent(false);
        private static R_C_Log log = new R_C_Log();
        private IPAddress HOST;
        private int PORT;
        private static SpeechSynthesizer Speeker = new SpeechSynthesizer();
        private delegate bool SendSort ( string Ticket );
        private TicketPrinter prienter = new TicketPrinter();
        private Dictionary<string, string> Info;
        private Socket Client;

        public void setInfo ( Dictionary<string, string> info ) {
            Info = info;
        }

        /// <summary>
        /// 初始化连接信息
        /// </summary>
        /// <param name="ConfigInfo"></param>
        public ClientSocket ( Dictionary<string, string> ConfigInfo ) {
            try {
                Info = ConfigInfo;
                this.HOST = IPAddress.Parse ( ConfigInfo["Host"] );
                this.PORT = Convert.ToInt32 ( ConfigInfo["Port"] );
            }
            catch ( Exception e ) {
                log.S_Log ( e.Message );
            }
        }

        public bool createTicket ( string Number ) {
            if (Info != null){
                try{
                    lock (prienter){
                        prienter.printTicket(Number, Info["Name"], Info["Org"]);
                        //SendSort S = new SendSort();
                    }
                    return true;
                }
                catch (Exception){
                    return false;
                }
            }
            else{
                return false;
            }
        }
        public void LinkServer ( ) {
            Socket Client = new Socket(SocketType.Stream,ProtocolType.Tcp);
            log.S_Log ( "链接服务器" );
            IPEndPoint IP = new IPEndPoint(HOST,PORT);
            StateObject State = new StateObject();
            try{
                Client.BeginConnect(IP,Connect,State);
            }
            catch ( Exception e ) {
                log.S_Log ( e.Message );
            }
        }

        private void Connect(IAsyncResult Result){
            Socket StepNow = (Socket) Result.AsyncState;
            StepNow.EndConnect(Result);
        }
    }
}
