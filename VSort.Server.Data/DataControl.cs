﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using VSort.Server.Loger;
using VSort.Server.Config;
using Newtonsoft.Json;

namespace VSort.Server.Data {
    public class DataControl {
        R_S_Log log = new R_S_Log();
        ConfigControl getConfig = new ConfigControl();
        public string Dbname, serverHost, userName, passWord;
        private MySqlConnection MySQL;
        public bool ISEXITDB=false;

        /// <summary>
        /// 初始化数据库连接
        /// </summary>
        /// <param name="N">数据库名称</param>
        /// <param name="H">数据库服务器地址</param>
        /// <param name="U">数据库用户名</param>
        /// <param name="P">数据库密码</param>
        /// <param name="PORT">数据库服务器端口</param>
        public DataControl ( string N, string H, string U, string P, uint PORT ) {
            //TODO:初始化数据库服务器连接参数
            try {
                Dbname = N;
                serverHost = H;
                userName = U;
                passWord = P;

                MySqlConnectionStringBuilder HostInfo = new MySqlConnectionStringBuilder();
                HostInfo.Server = serverHost;
                HostInfo.Port = PORT;
                HostInfo.UserID = userName;
                HostInfo.Password = passWord;
                HostInfo.Database = Dbname;
                MySQL = new MySqlConnection ( HostInfo.ConnectionString );
                MySQL.Open ( );
                //确保数据库存在
                createDatabase ( );
            }
            catch ( Exception e ) {
                log.error ( "数据库连接错误" + e.Message );
                throw;
            }
        }


        /// <summary>
        /// 插入排号数据
        /// </summary>
        /// <param name="sort">排号单号名称</param>
        /// <param name="window">窗口号，默认为A</param>
        /// <param name="level">客户等级，默认为1</param>
        public void setSort ( string sort, string window = "A", int level = 1 ) {
            MySqlCommand common = new MySqlCommand("",MySQL);
            common.CommandText = string.Format ( "INSERT INTO ticket (sort, time, window, level) VALUES ('{0}','{1}','{2}','{3}','{4}') ", sort, DateTime.Now, window, level );
            common.ExecuteNonQuery ( );
            common.Dispose ( );
        }

        /// <summary>
        /// 取得所有表单数据
        /// </summary>
        /// <returns></returns>
        public List<Dictionary<string, string>> getSortAll ( ) {
            MySqlCommand common = new MySqlCommand("",MySQL);
            common.CommandText = "SELECT * FROM ticket ORDER BY level ASC,id ASC";
            MySqlDataReader data = common.ExecuteReader();
            common.Dispose ( );
            List<Dictionary<string,string>> DataList = new List<Dictionary<string, string>>();
            DataList = null;
            string JsonData = "";
            try {
                while ( data.Read ( ) ) {
                    JsonData += JsonConvert.SerializeObject ( data );
                }

            }
            finally {
                data.Close ( );
            }
            return DataList;
        }

        /// <summary>
        /// 删除排号单
        /// </summary>
        /// <param name="Ticket"></param>
        public void deleteSort ( string Ticket ) {
            MySqlCommand common = new MySqlCommand("",MySQL);
            common.CommandText = string.Format ( "DELETE FROM ticket WHERE sort = {0}", Ticket );
            common.ExecuteNonQuery ( );
            common.Dispose ( );
        }
        private void createDatabase ( ) {
            MySqlCommand common = new MySqlCommand("",MySQL);
            common.CommandText = string.Format ( "CREATE DATABASE IF NOT EXITS `ticket` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci" );
            common.ExecuteNonQuery ( );
            common.CommandText = string.Format ( "CREATE TABLE IF NOT EXITS `sort` (`id`  int NULL AUTO_INCREMENT ,`sort`  bigint NULL ,`sort`  bigint NULL ,`window`  char(255) NULL ,`level`  int NULL ,`time`  bigint NULL ,PRIMARY KEY (`id`));" );
            common.ExecuteNonQuery ( );
            ISEXITDB = true;
            common.Dispose ( );
        }
        private bool firstUse ( ) {

            return false;
        }
        ~DataControl ( ) {
            MySQL.Close ( );
            MySQL.Dispose ( );
        }
    }
}
