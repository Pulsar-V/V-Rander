﻿using System;
using System.Collections.Generic;
using LitJson;
using System.IO;
using Newtonsoft.Json;
using System.Text;

namespace VSort.Server.Config {

    /// <summary>
    /// 用于加载和设置服务端配置文件
    /// </summary>
    public class ConfigControl {
        private Dictionary<string, string> ConfigInfo = new Dictionary<string, string>();
        static private string ConfigFilePath = @"Config/ServerInfo.json";
        static private string ConfigDictionary = @"Config";
        public Dictionary<string, string> getConfigInfo() {
            if (checkDictonary()) {
                var JsonData = File.OpenText(ConfigFilePath);
                var JsonStr = JsonData.ReadToEnd();
                string JsonString = JsonStr.ToString();
                }
            return null;
            }
        /// <summary>
        /// 设置配置文件
        /// </summary>
        /// <param name="Host"></param>
        /// <param name="Port"></param>
        /// <param name="Member"></param>
        /// <returns></returns>
        static public bool setConfigInfo(string setHost, string setPort, string setMember, string setDBname, string setPsw, string setDBhost = "127.0.0.1",string DBPort="3306") {
            JsonSerializer Json = new JsonSerializer();
            StringWriter str = new StringWriter();
            Newtonsoft.Json.JsonWriter JsonWrite = new JsonTextWriter(str);
            JsonWrite.WriteStartObject();
            JsonWrite.WritePropertyName("Port");
            JsonWrite.WriteValue(setPort);
            JsonWrite.WritePropertyName("ServerAddr");
            JsonWrite.WriteValue(setHost);
            JsonWrite.WritePropertyName("User");
            JsonWrite.WriteValue(setMember);
            JsonWrite.WritePropertyName("DBname");
            JsonWrite.WriteValue(setDBname);
            JsonWrite.WritePropertyName("Password");
            JsonWrite.WriteValue(setPsw);
            JsonWrite.WritePropertyName("DBPort");
            JsonWrite.WriteValue(DBPort);
            JsonWrite.WritePropertyName("DBhost");
            JsonWrite.WriteValue(setDBhost);
            JsonWrite.WriteEndObject();
            JsonWrite.Flush();
            string ConfigInfo = str.GetStringBuilder().ToString();
            if (checkDictonary()) {
                try {
                    Console.WriteLine(ConfigInfo);
                    File.WriteAllText(ConfigFilePath, ConfigInfo);
                    return true;
                    }
                catch {
                    return false;
                    }
                }
            else {
                return false;
                }
            }

        /// <summary>
        /// 用于检测配置目录和文件是否存在如果没有就新建一个
        /// </summary>
        /// <returns></returns>
        static private bool checkDictonary() {
            if (Directory.Exists(ConfigDictionary))
            {
                return (true);
            }
            else {
                Directory.CreateDirectory(ConfigDictionary);
                if (File.Exists(ConfigFilePath)) {
                    File.Create(ConfigFilePath);
                    return true;
                    }
                }
            return false;
            }
        static public JsonData loadConfig()
        {
            if (checkDictonary())
            {

                StreamReader sr = new StreamReader(ConfigFilePath, Encoding.Default);
                string json=sr.ReadToEnd();
                sr.Close();
                return JsonMapper.ToObject(json);
                
                
            }
            else return null;
        }
        }
}
