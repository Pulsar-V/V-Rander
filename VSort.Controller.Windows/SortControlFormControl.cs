﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using VSort.Controller.Config;
using System.Speech.Synthesis;
using Newtonsoft.Json;

namespace VSort.Controller.Windows {
    public partial class SortControlFormControl : Form {
        private ConfigControl Config = new ConfigControl();
        private SpeechSynthesizer Speeker = new SpeechSynthesizer();
        private List<Dictionary<string, string>> SortList;
        public SortControlFormControl() {
            FormClosed += SortControlFormControl_FormClosed;
            Load += SortControlFormControl_Load;
            Paint += SortControlFormControl_Paint;
            FormClosed += SortControlFormControl_FormClosed1;
            this.Opacity = 0.8;
            InitializeComponent();
            }

        private void SortControlFormControl_FormClosed1(object sender, FormClosedEventArgs e) {
            Speeker.Dispose();
            }


        /// <summary>
        /// 窗体重绘事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SortControlFormControl_Paint(object sender, PaintEventArgs e) {
            if (Config.IsSetToken()) {
                setTokenB.Text = "重设服务器Token";
                TokenContent.Text = Config.readToken();
                }
            else {
                setTokenB.Text = "设置Token";
                TokenContent.Text = "null";
                }
            }


        /// <summary>
        /// 窗体加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SortControlFormControl_Load(object sender, EventArgs e) {
            Dictionary<string, string> Info = new Dictionary<string, string>();
            Info = Config.getConfigInfo();
            Console.WriteLine();
            if (Config.IsSetToken()) {
                if (Config.readToken() == null) {
                    TokenContent.Text = Config.readToken();
                    }
                else
                    TokenContent.Text = "Unset Token";
                }
            refreshSortData();
            }

        private void updateListBoxToDic() {
            List<Dictionary<string, string>> Sort = new List<Dictionary<string, string>>();
            foreach (ListBox x in listBox1.Items) {

                }
            }

        /// <summary>
        /// 更新ITEM
        /// </summary>
        private void refreshSortData() {
            try {
                SortList = Config.readSortData();
                addMember(Config.readSortData());
                }
            catch (Exception) {
                Speeker.SpeakAsync("系统错误");
                }
            }

        /// <summary>
        /// 序列化读取json
        /// </summary>
        /// <param name="Member"></param>
        private void addMember(List<Dictionary<string, string>> Member) {
            foreach (Dictionary<string, string> Sort in Member) {
                listBox1.Items.Add(Sort["ticket"]);
                }
            }
        private void SortControlFormControl_FormClosed(object sender, FormClosedEventArgs e) {
            Application.Exit();
            }


        /// <summary>
        /// 服务器配置窗口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button7_Click(object sender, EventArgs e) {
            string Token = Interaction.InputBox("请输入服务器Token", "Token", "", 200, 100);
            Config.setLocalToken(Token);
            Refresh();
            }
        private void updateJsonData() {
            Config.updateSortData(JsonConvert.SerializeObject(SortList));
            }

        /// <summary>
        /// 点击呼叫
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e) {
            if (listBox1.SelectedItem != null) {
                Speeker.SpeakAsync("请客户" + listBox1.SelectedItem.ToString() + "到" + listBox1.SelectedItem.ToString().Substring(0, 1) + "窗口");
                }
            }


        /// <summary>
        /// 重呼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BReAsk_Click(object sender, EventArgs e) {
            Speeker.SpeakAsync("请客户" + SortList[0]["ticket"] + "到" + SortList[0]["ticket"].Substring(0, 1) + "窗口");
            }


        /// <summary>
        /// 刷新列表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BRefreshSort_Click(object sender, EventArgs e) {
            listBox1.Items.Clear();
            refreshSortData();
            }


        /// <summary>
        /// 下一个
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BNext_Click(object sender, EventArgs e) {
            SortList.RemoveAt(0);
            updateJsonData();
            listBox1.Items.Clear();
            refreshSortData();
            Speeker.SpeakAsync("请客户" + SortList[0]["ticket"] + "到" + SortList[0]["ticket"].Substring(0, 1) + "窗口");

            }


        /// <summary>
        /// 下移一位
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BDeleteSort_Click(object sender, EventArgs e) {
            if (listBox1.SelectedItem != null) {
                int index = listBox1.SelectedIndex;
                listBox1.Items.RemoveAt(index);
                SortList.RemoveAt(index);
                updateJsonData();
                }
            }


        /// <summary>
        /// 上移
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BTurnDown_Click(object sender, EventArgs e) {
            if (listBox1.SelectedItem != null) {
                int index = listBox1.SelectedIndex;
                string content = listBox1.SelectedItem.ToString();
                if (index != SortList.Count-1) {
                    listBox1.Items.Insert(index + 2, content);
                    listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                    Dictionary<string, string> Temp = new Dictionary<string, string>();
                    Temp = SortList[index + 1];
                    SortList[index + 1] = SortList[index];
                    SortList[index] = Temp;
                    updateJsonData();
                    }
                }
            }

        private void BTurnUP_Click(object sender, EventArgs e) {
            if (listBox1.SelectedItem != null) {
                int index = listBox1.SelectedIndex;
                string content = listBox1.SelectedItem.ToString();
                if (index != 0) {
                    listBox1.Items.Insert(index - 1, content);
                    listBox1.Items.RemoveAt(listBox1.SelectedIndex);
                    Dictionary<string, string> Temp = new Dictionary<string, string>();
                    Temp = SortList[index - 1];
                    SortList[index - 1] = SortList[index];
                    SortList[index] = Temp;
                    updateJsonData();
                    }
                }
            }

        private void BServerLink_Click(object sender, EventArgs e) {

            }
        }
    }
