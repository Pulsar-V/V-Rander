﻿using System;
using System.Windows.Forms;
using VSort.Controller.Config;

namespace VSort.Controller.Windows {
    public partial class SortControlFormLogin : Form {
        private ConfigControl Config = new ConfigControl();
        SortControlFormControl MainForm;
        public SortControlFormLogin(SortControlFormControl MainForm) {
            this.MainForm = MainForm;
            InitializeComponent();
            }

        private void button1_Click(object sender, EventArgs e) {
            Config.setConfigInfo(textHost.Text,textPort.Text);
            Hide();
            MainForm.Show();
            }
        }
    }
