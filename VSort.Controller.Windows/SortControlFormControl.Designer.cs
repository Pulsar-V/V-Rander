﻿namespace VSort.Controller.Windows {
    partial class SortControlFormControl {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
                }
            base.Dispose(disposing);
            }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.BReAsk = new System.Windows.Forms.Button();
            this.BNext = new System.Windows.Forms.Button();
            this.BDeleteSort = new System.Windows.Forms.Button();
            this.BRefreshSort = new System.Windows.Forms.Button();
            this.BTurnUP = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SortNow = new System.Windows.Forms.Label();
            this.StatuesNow = new System.Windows.Forms.Label();
            this.setTokenB = new System.Windows.Forms.Button();
            this.TokenContent = new System.Windows.Forms.Label();
            this.BTurnDown = new System.Windows.Forms.Button();
            this.BServerLink = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("宋体", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 20;
            this.listBox1.Location = new System.Drawing.Point(12, 13);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(467, 364);
            this.listBox1.TabIndex = 0;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // BReAsk
            // 
            this.BReAsk.Location = new System.Drawing.Point(548, 13);
            this.BReAsk.Name = "BReAsk";
            this.BReAsk.Size = new System.Drawing.Size(134, 32);
            this.BReAsk.TabIndex = 1;
            this.BReAsk.Text = "重    呼";
            this.BReAsk.UseVisualStyleBackColor = true;
            this.BReAsk.Click += new System.EventHandler(this.BReAsk_Click);
            // 
            // BNext
            // 
            this.BNext.Location = new System.Drawing.Point(548, 51);
            this.BNext.Name = "BNext";
            this.BNext.Size = new System.Drawing.Size(134, 32);
            this.BNext.TabIndex = 2;
            this.BNext.Text = "下 一 个";
            this.BNext.UseVisualStyleBackColor = true;
            this.BNext.Click += new System.EventHandler(this.BNext_Click);
            // 
            // BDeleteSort
            // 
            this.BDeleteSort.Location = new System.Drawing.Point(548, 89);
            this.BDeleteSort.Name = "BDeleteSort";
            this.BDeleteSort.Size = new System.Drawing.Size(134, 31);
            this.BDeleteSort.TabIndex = 3;
            this.BDeleteSort.Text = "删除排号";
            this.BDeleteSort.UseVisualStyleBackColor = true;
            this.BDeleteSort.Click += new System.EventHandler(this.BDeleteSort_Click);
            // 
            // BRefreshSort
            // 
            this.BRefreshSort.Location = new System.Drawing.Point(548, 126);
            this.BRefreshSort.Name = "BRefreshSort";
            this.BRefreshSort.Size = new System.Drawing.Size(134, 29);
            this.BRefreshSort.TabIndex = 4;
            this.BRefreshSort.Text = "刷新列表";
            this.BRefreshSort.UseVisualStyleBackColor = true;
            this.BRefreshSort.Click += new System.EventHandler(this.BRefreshSort_Click);
            // 
            // BTurnUP
            // 
            this.BTurnUP.Location = new System.Drawing.Point(548, 161);
            this.BTurnUP.Name = "BTurnUP";
            this.BTurnUP.Size = new System.Drawing.Size(134, 29);
            this.BTurnUP.TabIndex = 6;
            this.BTurnUP.Text = "上    移";
            this.BTurnUP.UseVisualStyleBackColor = true;
            this.BTurnUP.Click += new System.EventHandler(this.BTurnUP_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(529, 247);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 7;
            this.label1.Text = "当前服务号";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(559, 280);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 12);
            this.label2.TabIndex = 8;
            this.label2.Text = "Token";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(541, 310);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 9;
            this.label3.Text = "当前状态";
            // 
            // SortNow
            // 
            this.SortNow.AutoSize = true;
            this.SortNow.Location = new System.Drawing.Point(625, 247);
            this.SortNow.Name = "SortNow";
            this.SortNow.Size = new System.Drawing.Size(59, 12);
            this.SortNow.TabIndex = 10;
            this.SortNow.Text = "123445324";
            // 
            // StatuesNow
            // 
            this.StatuesNow.AutoSize = true;
            this.StatuesNow.Location = new System.Drawing.Point(630, 310);
            this.StatuesNow.Name = "StatuesNow";
            this.StatuesNow.Size = new System.Drawing.Size(29, 12);
            this.StatuesNow.TabIndex = 12;
            this.StatuesNow.Text = "异常";
            // 
            // setTokenB
            // 
            this.setTokenB.Location = new System.Drawing.Point(620, 342);
            this.setTokenB.Name = "setTokenB";
            this.setTokenB.Size = new System.Drawing.Size(123, 23);
            this.setTokenB.TabIndex = 13;
            this.setTokenB.Text = "设置Token";
            this.setTokenB.UseVisualStyleBackColor = true;
            this.setTokenB.Click += new System.EventHandler(this.button7_Click);
            // 
            // TokenContent
            // 
            this.TokenContent.AutoSize = true;
            this.TokenContent.Location = new System.Drawing.Point(630, 280);
            this.TokenContent.Name = "TokenContent";
            this.TokenContent.Size = new System.Drawing.Size(17, 12);
            this.TokenContent.TabIndex = 14;
            this.TokenContent.Text = "无";
            // 
            // BTurnDown
            // 
            this.BTurnDown.Location = new System.Drawing.Point(548, 196);
            this.BTurnDown.Name = "BTurnDown";
            this.BTurnDown.Size = new System.Drawing.Size(134, 29);
            this.BTurnDown.TabIndex = 15;
            this.BTurnDown.Text = "下    移";
            this.BTurnDown.UseVisualStyleBackColor = true;
            this.BTurnDown.Click += new System.EventHandler(this.BTurnDown_Click);
            // 
            // BServerLink
            // 
            this.BServerLink.Location = new System.Drawing.Point(491, 342);
            this.BServerLink.Name = "BServerLink";
            this.BServerLink.Size = new System.Drawing.Size(123, 23);
            this.BServerLink.TabIndex = 16;
            this.BServerLink.Text = "连接服务器";
            this.BServerLink.UseVisualStyleBackColor = true;
            this.BServerLink.Click += new System.EventHandler(this.BServerLink_Click);
            // 
            // SortControlFormControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(755, 393);
            this.Controls.Add(this.BServerLink);
            this.Controls.Add(this.BTurnDown);
            this.Controls.Add(this.TokenContent);
            this.Controls.Add(this.setTokenB);
            this.Controls.Add(this.StatuesNow);
            this.Controls.Add(this.SortNow);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BTurnUP);
            this.Controls.Add(this.BRefreshSort);
            this.Controls.Add(this.BDeleteSort);
            this.Controls.Add(this.BNext);
            this.Controls.Add(this.BReAsk);
            this.Controls.Add(this.listBox1);
            this.Name = "SortControlFormControl";
            this.Text = "排号控制";
            this.ResumeLayout(false);
            this.PerformLayout();

            }

        #endregion
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Button BReAsk;
        private System.Windows.Forms.Button BNext;
        private System.Windows.Forms.Button BDeleteSort;
        private System.Windows.Forms.Button BRefreshSort;
        private System.Windows.Forms.Button BTurnUP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label SortNow;
        private System.Windows.Forms.Label StatuesNow;
        private System.Windows.Forms.Button setTokenB;
        private System.Windows.Forms.Label TokenContent;
        private System.Windows.Forms.Button BTurnDown;
        private System.Windows.Forms.Button BServerLink;
        }
    }