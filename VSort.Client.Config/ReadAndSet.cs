﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using VSort.Client.Loger;

namespace VSort.Client.Config {

    /// <summary>
    /// 获取和设置配置文件
    /// </summary>
    public class GetSetConfig {
        private R_C_Log log = new R_C_Log();
        private readonly string ConfigXmlpath = @"./config/ClientConfig.xml";
        private readonly string ConfigDirectorypath = @"config";

        /// <summary>
        /// 初始化配置文件
        /// </summary>
        public GetSetConfig() {
            checkDictory();
            }
        public void checkDictory() {
            if (Directory.Exists(ConfigDirectorypath)) {
                Directory.CreateDirectory(ConfigDirectorypath);
                }
            }
        public bool checkConfigFile() {
            checkDictory();
            return File.Exists(ConfigXmlpath);
            }
        /// <summary>
        /// 读取配置文件
        /// </summary>
        /// <returns>配置文件信息集合</returns>
        public Dictionary<string, string> Read() {
            try {
                XmlDocument Config = new XmlDocument();
                Config.Load(ConfigDirectorypath);
                Dictionary<string, string> Info = new Dictionary<string, string>();
                Info["Host"] = Config.SelectSingleNode("/Config/Host").InnerText;
                Info["Port"] = Config.SelectSingleNode("/Config/Port").InnerText;
                Info["Uname"] = Config.SelectSingleNode("/Config/Uname").InnerText;
                Info["Psw"] = Config.SelectSingleNode("/Config/Psw").InnerText;
                return Info;
                }
            catch (Exception e) {
                log.S_Log(e.Message);
                return null;
                }
            }

        /// <summary>
        /// 设置Config文件
        /// </summary>
        /// <param name="Uname"></param>
        /// <param name="Psw"></param>
        /// <param name="Host"></param>
        /// <param name="Port"></param>
        /// <returns></returns>
        public bool Set(string Uname, string Psw, string Host, string Port) {
            try {
                checkDictory();
                CreateXmlFile(Uname, Psw, Host, Port);
                return true;
                }
            catch (Exception e) {
                log.S_Log(e.Message);
                return false;
                }
            }

        /// <summary>
        /// 将配置信息写入xml配置文件
        /// </summary>
        /// <param name="Uname"></param>
        /// <param name="Psw"></param>
        /// <param name="Host"></param>
        /// <param name="Port"></param>
        private void CreateXmlFile(
            string Uname = null,
            string Psw = null,
            string Host = null,
            string Port = null) {
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode node = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", "");
            xmlDoc.AppendChild(node);
            XmlNode root = xmlDoc.CreateElement("Config");
            xmlDoc.AppendChild(root);
            CreateNode(xmlDoc, root, "Host", Host);
            CreateNode(xmlDoc, root, "Port", Port);
            CreateNode(xmlDoc, root, "Uname", Uname);
            CreateNode(xmlDoc, root, "Psw", Psw);
            try {
                xmlDoc.Save(ConfigXmlpath);
                }
            catch (Exception e) {
                log.S_Log(e.ToString());
                }
            }

        /// <summary>    
        /// 创建节点    
        /// </summary>
        private void CreateNode(XmlDocument xmlDoc, XmlNode parentNode, string name, string value) {
            XmlNode node = xmlDoc.CreateNode(XmlNodeType.Element, name, null);
            node.InnerText = value;
            parentNode.AppendChild(node);
            }
        }
    }
