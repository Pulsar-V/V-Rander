﻿using System;
using System.IO;
using System.Text;

namespace VSort.Controller.Log {
    public class ControllerLog {
        private string Log_Dictory = "log";
        private string Time = DateTime.Now.ToLongDateString();
        private string Log_File_Path = "./log/" + DateTime.Now.ToLongDateString() + ".log";
        private string ErrorLogFilePath = "./log/" + "Error" + DateTime.Now.ToLongDateString() + ".log";
        /// <summary>
        /// 日志目录检测
        /// </summary>
        /// <returns></returns>
        private bool Create_Directory() {
            try {
                if (Directory.Exists(Log_Dictory)) {
                    return true;
                    }
                else {
                    Directory.CreateDirectory(Log_Dictory);
                    return true;
                    }
                }
            catch (Exception) {
                return false;
                }
            }

        /// <summary>
        /// 写入运行日志
        /// </summary>
        /// <param name="Data"></param>
        /// <returns></returns>
        public bool log(string Data) {
            if (Create_Directory()) {
                Data = string.Format("Time:{0}\nUser: {1}\nOS:{2}\nInfo:\n{3}\n\n", Time, Environment.UserName, Environment.OSVersion, Data);
                try {
                    StreamWriter F_Writer = new StreamWriter(Log_File_Path, true, Encoding.UTF8);
                    F_Writer.Write(Data);
                    F_Writer.Close();
                    return true;
                    }
                catch (Exception) {
                    return false;
                    }
                }
            else {
                return false;
                }
            }

        /// <summary>
        /// 写入错误日志
        /// </summary>
        /// <param name="Data"></param>
        /// <returns></returns>
        public bool error(string Data) {
            if (Create_Directory()) {
                Data = string.Format("Time:{0}\nUser: {1}\nOS:{2}\nInfo:\n{3}\n\n", Time, Environment.UserName, Environment.OSVersion, Data);
                try {
                    StreamWriter F_Writer = new StreamWriter(Log_File_Path, true, Encoding.UTF8);
                    F_Writer.Write(Data);
                    F_Writer.Close();
                    return true;
                    }
                catch (Exception) {
                    return false;
                    }
                }
            else {
                return false;
                }
            }
        }
    }
