﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using VSort.Controller.Log;

namespace VSort.Controller.Server {
    public class StateObject {
        public Socket WorkSocket = null;
        public const int BufferSize = 1024;
        public byte[] buffer = new byte[BufferSize];
        public string Content;
        }
    public class Controller_Socket
    {
        private string Token { get; set; }
        private ControllerLog log = new ControllerLog();
        /// <summary>
        /// 构造连接
        /// </summary>
        /// <param name="HOST"></param>
        /// <param name="PORT"></param>
        /// <returns></returns>
        private Socket CreateSocket(string HOST,string PORT) {
            try {
                Socket Connect = new Socket(AddressFamily.NetBios ,SocketType.Stream, ProtocolType.Tcp);
                Connect.Bind(new IPEndPoint(IPAddress.Parse(HOST), Convert.ToInt32(PORT)));
                return Connect;
                }
            catch {
                log.error("创建连接失败");
                return null;
                }
            }
        /// <summary>
        /// 连接服务器
        /// </summary>
        /// <param name="HOST"></param>
        /// <param name="PORT"></param>
        public void Connect(string HOST, string PORT) {
            Socket Connected = CreateSocket(HOST, PORT);
            if (Connected != null) {
               
                }
            else {
                Thread.Sleep(100);
                Connect(HOST,PORT);
                }
            }

        /// <summary>
        /// 发送数据到服务器数据库
        /// </summary>
        public void sendData(IAsyncResult Result) {
            if (Token!=null) {

                }
            }


        /// <summary>
        /// 加载TOKEN
        /// </summary>
        /// <param name="Key"></param>
        /// <returns></returns>
        private bool loadKey(string Key) {
            Token = Key;
            return true;
            }

    }
}
