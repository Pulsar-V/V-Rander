﻿using System;
using System.Collections.Generic;
using System.Windows;
using VSort.Client.Loger;
using VSort.Client.Server;
using System.Speech.Synthesis;
using System.Threading;
using System.Windows.Controls;
using VSort.Client.Config;

namespace VSort.Client.Windows {

    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window {
        public static R_C_Log log = new R_C_Log();
        private static SpeechSynthesizer Speeker = new SpeechSynthesizer();
        private List<Thread> Thread_Pool = new List<Thread>();
        private GetSetConfig Config = new GetSetConfig();
        private Dictionary<string, string> Info = new Dictionary<string, string>();
        private int LinkTimeOut = 0;
        private delegate void setLinkStatues ( string str );

        public MainWindow ( ) {
            Loaded += MainWindow_Loaded;
            Closed += MainWindow_Closed;
            Speeker.SpeakAsync ( "加载客户端" );
            WindowState = WindowState.Maximized;
            InitializeComponent ( );
        }
        private void MainWindow_Loaded ( object sender, RoutedEventArgs e ) {
            log.S_Log ( "启动客户端" );
            this.BGetTicket.Width = this.Width;
            GStartUI.Visibility = Visibility.Collapsed;
            GClientUI.Visibility = Visibility.Collapsed;
            BReLink.Visibility = Visibility.Collapsed;//隐藏重连按钮
            new Thread(new ThreadStart(() => {
                setup();
            }));
        }
        private void setStatueText ( string str ) {
            LinkStatues.Text = str;
        }

        private void setup ( ){
            if (LinkTimeOut < 10){
                Info = Config.Read();
                if (Info != null){
                }
                else{
                    GStartUI.Visibility = Visibility.Visible;
                }
            }
            else{
                setLinkStatues Text = new setLinkStatues(setStatueText);
                Text.Invoke("连接超时！！！");
            }
        }

        private delegate void StartLink ( string Host, int Port );

        private void Link ( string Host, int Port ) {
            Dictionary<string, string> ConfigInfo=new Dictionary<string, string>();
            ConfigInfo["Host"] = Host;
            ConfigInfo["Port"] = Port.ToString();
            ClientSocket ClientLink = new ClientSocket(ConfigInfo);

        }

        private void MainWindow_Closed ( object sender, EventArgs e ) {
            if ( Thread_Pool.Count > 0 ) {
                foreach ( Thread Threader in Thread_Pool ) {
                    Threader.Abort ( );
                }
            }
        }

        private void BGetTicket_Click ( object sender, RoutedEventArgs e ) {

        }
    }
}
