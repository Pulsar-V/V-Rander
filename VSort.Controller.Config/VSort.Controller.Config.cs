﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace VSort.Controller.Config {
    /// <summary>
    /// 用于加载和设置服务端配置文件
    /// </summary>
    public class ConfigControl {
        private Dictionary<string, string> ConfigInfo = new Dictionary<string, string>();
        public readonly static string ConfigFilePath = @"Config/ControllerInfo.json";
        public readonly static string ConfigDictionary = @"Config";
        public readonly static string DataDir = @"Data";
        public readonly static string TokenPath = @"Data/Token.json";
        public readonly static string SortDataPath=@"Data/SortData.json";
        public readonly static int BufferSize = 1024;


        public Dictionary<string , string> getConfigInfo ( ) {
            Dictionary<string , string> info = new Dictionary<string , string> ( );
            string Read = "";
            if ( checkDictonary ( ) ) {
                lock ( this ) {
                    byte [ ] Buffer = new byte [ BufferSize ];
                    FileStream ConfigFile = new FileStream ( ConfigFilePath , FileMode.Open , FileAccess.Read );

                    while ( ConfigFile.Read ( Buffer , 0 , BufferSize ) > 0 ) {
                        Read += Encoding.UTF8.GetString ( Buffer ).Trim ( '\0' );
                    }
                    try {
                        JObject JsonObject = JObject.Parse ( Read );
                        info["Host"] = JsonObject.GetValue ( "Host" ).ToString ( );
                        info [ "Port" ] = JsonObject.GetValue ( "Port" ).ToString ( );
                        ConfigFile.Close ( );
                        return info;

                    } catch ( Exception ) {
                        ConfigFile.Close ( );
                        return null;
                    }
                }
            }
            return null;
        }
        /// <summary>
        /// 设置配置文件
        /// </summary>
        /// <param name="Host"></param>
        /// <param name="Port"></param>
        /// <param name="Member"></param>
        /// <returns></returns>
        public bool setConfigInfo ( string setHost , string setPort ) {
            JsonSerializer Json = new JsonSerializer ( );
            StringWriter str = new StringWriter ( );
            JsonWriter JsonWrite = new JsonTextWriter ( str );
            JsonWrite.WriteStartObject ( );
            JsonWrite.WritePropertyName ( "Host" );
            JsonWrite.WriteValue ( setHost );
            JsonWrite.WritePropertyName ( "Port" );
            JsonWrite.WriteValue ( setPort );
            JsonWrite.WriteEndObject ( );
            JsonWrite.Flush ( );
            string ConfigInfo = str.GetStringBuilder ( ).ToString ( );
            if ( checkDictonary ( ) ) {
                try {
                    FileStream ConfigFile = new FileStream ( ConfigFilePath , FileMode.Create );
                    //Console.WriteLine ( ConfigInfo );
                    byte [ ] info = Encoding.UTF8.GetBytes ( ConfigInfo );
                    ConfigFile.Write ( info , 0 , info.Length );
                    ConfigFile.Close ( );
                    return true;
                } catch {
                    return false;
                }
            } else {
                return false;
            }
        }

        /// <summary>
        /// 用于检测配置目录和文件是否存在如果没有就新建一个
        /// </summary>
        /// <returns>true</returns>
        public bool checkDictonary ( ) {
            if ( Directory.Exists ( ConfigDictionary ) && Directory.Exists ( TokenPath ) )
                return ( true );
            else {
                Directory.CreateDirectory ( ConfigDictionary );
                Directory.CreateDirectory ( DataDir );
                return true;
            }
        }
        public bool IsSetToken ( ) {
            try {
                FileStream TokenFile = new FileStream ( TokenPath , FileMode.Open );
                TokenFile.Close ( );
                return true;
            } catch ( Exception ) {

                return false;
            }
        }

        /// <summary>
        /// 设置本地TOKEN
        /// </summary>
        /// <param name="Token"></param>
        public void setLocalToken ( string Token ) {
            FileStream TokenFile = new FileStream ( TokenPath , FileMode.Create , FileAccess.Write );
            StringWriter str = new StringWriter ( );
            JsonWriter JsonWrite = new JsonTextWriter ( str );
            TokenFile.Flush ( );
            JsonWrite.WriteStartObject ( );
            JsonWrite.WritePropertyName ( "Token" );
            JsonWrite.WriteValue ( Token );
            JsonWrite.WriteEndObject ( );
            JsonWrite.Flush ( );
            string TokenInfo = str.GetStringBuilder ( ).ToString ( );
            byte [ ] info = Encoding.UTF8.GetBytes ( TokenInfo );
            TokenFile.Write ( info , 0 , info.Length );
            TokenFile.Close ( );
        }

        /// <summary>
        /// 读取本地TOKEN
        /// </summary>
        /// <returns>string Token字符串</returns>
        public string readToken ( ) {
            string Token = null;
            lock ( this ) {
                if ( File.Exists ( TokenPath ) ) {
                    byte [ ] Json = new byte [ BufferSize ];
                    FileStream TokenFile = new FileStream ( TokenPath , FileMode.Open , FileAccess.Read );
                    TokenFile.Read ( Json , 0 , BufferSize );
                    Token = Encoding.UTF8.GetString ( Json ).Trim ( '\0' );
                    try {
                        JObject JsonObject = JObject.Parse ( Token );
                        Token = JsonObject.GetValue ( "Token" ).ToString ( );
                        TokenFile.Close ( );
                        return Token;
                    } catch ( Exception ) {
                        TokenFile.Close ( );
                        return null;
                    }
                } else {
                    return null;
                }
            }
        }


        /// <summary>
        /// 读取json
        /// </summary>
        /// <returns>
        /// List<Dictionary<stringstring>>类型的树状数据
        /// </returns>
        public List<Dictionary<string , string>> readSortData ( ) {
            lock ( this ) {
                if ( File.Exists ( SortDataPath ) ) {
                    byte [ ] Json = new byte [ BufferSize ];
                    string SortDataString = "";
                    FileStream SortDataFile = new FileStream ( SortDataPath , FileMode.Open , FileAccess.Read );
                    List<Dictionary<string , string>> SortData = new List<Dictionary<string , string>> ( );
                    while ( SortDataFile.Read ( Json , 0 , BufferSize ) > 0 ) {
                        SortDataString += Encoding.UTF8.GetString ( Json ).Trim ( '\0' );
                    }
                    //Console.WriteLine ( SortDataString );
                    try {
                        JsonSerializer Serializer = new JsonSerializer ( );
                        JsonReader ReadJson = new JsonTextReader ( new StringReader ( SortDataString ) );
                        SortData = Serializer.Deserialize<List<Dictionary<string , string>>> ( ReadJson );
                        SortDataFile.Close ( );
                        return SortData;
                    } catch ( Exception ) {
                        SortDataFile.Close ( );
                        return null;
                    }
                } else {
                    return null;
                }
            }
        }
        public void updateSortData (string str  ) {
            FileStream SortDataFile = new FileStream ( SortDataPath , FileMode.Create , FileAccess.Write );
            byte [ ] info = Encoding.UTF8.GetBytes ( str );
            SortDataFile.Write ( info , 0 , info.Length );
            SortDataFile.Close ( );
        }
    }
}
